## A simple `echo` function ruby function to get started with GitLab Serverless.

See the [documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) for more information.

### Usage

This function will echo back the environment for the variable located in the  `/echo-rb/echo.rb` file.

Run using curl (replace URL with your Knative service endpoint URL)

`curl -X POST http://functions-echo-rb.functions-10069011.example.com`